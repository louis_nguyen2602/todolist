import React, { useState } from 'react';
import TodoCreator from './TodoCreator';
import TodoRow from './TodoRow';
import VisibilityControl from './VisibilityControl';

function App() {
  let initTodoItems = [
      { action: "Buy Flowers", done: false },
      { action: "learn english", done: false },
      { action: "do exercise", done: true },
      { action: "go to market", done: false }
    ];
  const [todoItems, setTodoItems ] = useState(initTodoItems);
  const [showCompleted, setShowCompleted] = useState(false);

  const createNewTodo = (item) => {
    setTodoItems([...todoItems, {action: item, done: false}]);
  }

  const toggleTodo = (todo) => {
    let newStateTodoItems = todoItems.map(item => 
      item.action === todo.action ? {...item, done: !item.done} : item);
    setTodoItems(newStateTodoItems);
  }

  const todoTableRows = (doneValue) => {
    return todoItems.filter(item => item.done === doneValue).map(item => 
      <TodoRow key={item.action} item={item} callback={toggleTodo} />
    )
  }

  return (
    <div>
      <h4 className="bg-primary text-white text-center p-2">
        Your To Do List 
        ({todoItems.filter(t => !t.done).length} items to do)
      </h4>
      <div className="container-fluid">
        <TodoCreator callback={createNewTodo} />
        <table className="table table-striped table-bordered">
          <thead>
            <tr><th>Description</th><th>Done</th></tr>
          </thead>
          <tbody>{ todoTableRows(false) }</tbody>
        </table>
        <div className="bg-secondary text-white text-center p-2">
          <VisibilityControl  isChecked={showCompleted} callback={ (checked) => 
            setShowCompleted(checked)} />
        </div>
        { showCompleted &&                       
            <table className="table table-striped table-bordered">
              <thead>
                <tr><th>Description</th><th>Done</th></tr>
              </thead>
              <tbody>{ todoTableRows(true) }</tbody>
            </table>
        } 
      </div>
  </div>
  );
}

export default App;
