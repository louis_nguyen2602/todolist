import React from 'react';

const VisibilityControl = ({isChecked, callback}) => {

    return (
        <div className="form-check">
            <input className="form-check-input" type="checkbox" checked={isChecked}
                onChange={ (event) => callback(event.target.checked) } />
            <label className="form-check-label">
                Show completed tasks
            </label>
        </div>
    )
                        
}

export default VisibilityControl;