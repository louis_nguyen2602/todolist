import React, { useState } from 'react';

const TodoCreator = ({callback}) => {
    const [newItem, setNewItem] = useState('');

    const updateNewItem = (event) => {
        setNewItem(event.target.value);
    }

    const createNewTodo = () => {
        callback(newItem);
        setNewItem('');
    }

    return (
        <div className="my-1">
            <input className="form-control" placeholder="add new task" value={newItem} onChange={updateNewItem} />
            <button className="btn btn-primary mt-1" onClick={createNewTodo}>Add</button>
        </div>
    )               
}

export default TodoCreator;